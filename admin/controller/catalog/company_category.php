<?php
class ControllerCatalogCompanyCategory extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('catalog/company_category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/company_category');

        $this->getList();
    }

    public function add() {
        $this->load->language('catalog/company_category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/company_category');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_company_category->addCategory($this->request->post);

            $this->session->data['success'] = 'Success: You added the category successfully';

            $this->response->redirect($this->url->link('catalog/company_category', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function edit() {
        $this->load->language('catalog/company_category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/company_category');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_company_category->editCategory($this->request->get['category_id'], $this->request->post);

            $this->session->data['success'] = 'Success: You edited the category successfully';

            $this->response->redirect($this->url->link('catalog/company_category', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('catalog/company_category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/company_category');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $category_id) {
                $this->model_catalog_company_category->deleteCategory($category_id);
            }

            $this->session->data['success'] = 'Delete Success!';

            $this->response->redirect($this->url->link('catalog/company_category', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getList();
    }

    protected function getList() {
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/company_category', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['add'] = $this->url->link('catalog/company_category/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete'] = $this->url->link('catalog/company_category/delete', 'user_token=' . $this->session->data['user_token'], true);

        $data['categories'] = array();

        $results = $this->model_catalog_company_category->getCategories();

        foreach ($results as $result) {
            $data['categories'][] = array(
                'category_id' => $result['id_company_category'],
                'name'        => $result['company_category_name'],
                'edit'        => $this->url->link('catalog/company_category/edit', 'user_token=' . $this->session->data['user_token'] . '&category_id=' . $result['id_company_category'], true),
                'delete'      => $this->url->link('catalog/company_category/delete', 'user_token=' . $this->session->data['user_token'] . '&category_id=' . $result['id_company_category'], true)
            );
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/company_category_list', $data));
    }

    protected function getForm() {
        $data['text_form'] = !isset($this->request->get['category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        if (isset($this->error['name-ar'])) {
            $data['error_name_ar'] = $this->error['name-ar'];
        } else {
            $data['error_name_ar'] = array();
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/company_category', 'user_token=' . $this->session->data['user_token'], true)
        );

        if (!isset($this->request->get['category_id'])) {
            $data['action'] = $this->url->link('catalog/company_category/add', 'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action'] = $this->url->link('catalog/company_category/edit', 'user_token=' . $this->session->data['user_token'] . '&category_id=' . $this->request->get['category_id'], true);
        }

        $data['cancel'] = $this->url->link('catalog/company_category', 'user_token=' . $this->session->data['user_token'], true);

        if (isset($this->request->get['category_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $category_info = $this->model_catalog_company_category->getCategory($this->request->get['category_id']);
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($category_info)) {
            $data['name'] = $category_info[1];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['name-ar'])) {
            $data['name_ar'] = $this->request->post['name-ar'];
        } elseif (!empty($category_info)) {
            $data['name_ar'] = $category_info[2];
        } else {
            $data['name_ar'] = '';
        }

        $data['user_token'] = $this->session->data['user_token'];


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/company_category_form', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'catalog/company_category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 255)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if ((utf8_strlen($this->request->post['name-ar']) < 1) || (utf8_strlen($this->request->post['name-ar']) > 255)) {
            $this->error['name-ar'] = $this->language->get('error_name');
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'catalog/company_category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('catalog/company_category');

        foreach ($this->request->post['selected'] as $category_id) {
            $company_total = $this->model_catalog_company_category->getTotalCompaniesByCategoryId($category_id);

            if ($company_total) {
                $this->error['warning'] = 'There might be companies linked to any of these categories, so you can not delete them';
            }
        }

        return !$this->error;
    }

}

<?php
class ControllerCatalogSupplierRegistration extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('catalog/supplier_registration');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/supplier_registration');

        $this->getList();
    }

    public function add() {
        $this->load->language('catalog/supplier_registration');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/supplier_registration');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_supplier_registration->addSupplierRegistration($this->request->post);

            $this->session->data['success'] = 'Success: You added the supplier registration successfully';

            $this->response->redirect($this->url->link('catalog/supplier_registration', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function edit() {
        $this->load->language('catalog/supplier_registration');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/supplier_registration');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_supplier_registration->editSupplierRegistration($this->request->get['supplier_registration_id'], $this->request->post);

            $this->session->data['success'] = 'Success: You edited the supplier registration successfully';

            $this->response->redirect($this->url->link('catalog/supplier_registration', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('catalog/supplier_registration');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/supplier_registration');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $supplier_registration_id) {
                $this->model_catalog_supplier_registration->deleteSupplierRegistration($supplier_registration_id);
            }

            $this->session->data['success'] = 'Delete Success!';

            $this->response->redirect($this->url->link('catalog/supplier_registration', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getList();
    }

    protected function getList() {
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/supplier_registration', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['add'] = $this->url->link('catalog/supplier_registration/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete'] = $this->url->link('catalog/supplier_registration/delete', 'user_token=' . $this->session->data['user_token'], true);

        $data['categories'] = array();

        $results = $this->model_catalog_supplier_registration->getSupplierRegistrations();

        foreach ($results as $result) {
            $data['supplier_registrations'][] = array(
                'supplier_registration_id' => $result['supplier_registration_id'],
                'title'        => $result['title'],
                'edit'        => $this->url->link('catalog/supplier_registration/edit', 'user_token=' . $this->session->data['user_token'] . '&supplier_registration_id=' . $result['supplier_registration_id'], true),
                'delete'      => $this->url->link('catalog/supplier_registration/delete', 'user_token=' . $this->session->data['user_token'] . '&supplier_registration_id=' . $result['supplier_registration_id'], true)
            );
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/supplier_registration_list', $data));
    }

    protected function getForm() {
        $data['text_form'] = !isset($this->request->get['supplier_registration_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['title'])) {
            $data['error_title'] = $this->error['title'];
        } else {
            $data['error_title'] = '';
        }

        if (isset($this->error['title-ar'])) {
            $data['error_title_ar'] = $this->error['title-ar'];
        } else {
            $data['error_title_ar'] = '';
        }

        if (isset($this->error['published'])) {
            $data['error_published'] = $this->error['published'];
        } else {
            $data['error_published'] = '';
        }

        if (isset($this->error['published-ar'])) {
            $data['error_published_ar'] = $this->error['published-ar'];
        } else {
            $data['error_published_ar'] = '';
        }

        if (isset($this->error['activity'])) {
            $data['error_activity'] = $this->error['activity'];
        } else {
            $data['error_activity'] = '';
        }

        if (isset($this->error['activity-ar'])) {
            $data['error_activity_ar'] = $this->error['activity-ar'];
        } else {
            $data['error_activity_ar'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/supplier_registration', 'user_token=' . $this->session->data['user_token'], true)
        );

        if (!isset($this->request->get['supplier_registration_id'])) {
            $data['action'] = $this->url->link('catalog/supplier_registration/add', 'user_token=' . $this->session->data['user_token'], true);
        } else {
            $data['action'] = $this->url->link('catalog/supplier_registration/edit', 'user_token=' . $this->session->data['user_token'] . '&supplier_registration_id=' . $this->request->get['supplier_registration_id'], true);
        }

        $data['cancel'] = $this->url->link('catalog/supplier_registration', 'user_token=' . $this->session->data['user_token'], true);

        if (isset($this->request->get['supplier_registration_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $supplier_registration_info = $this->model_catalog_supplier_registration->getSupplierRegistration($this->request->get['supplier_registration_id']);
        }

        if (isset($this->request->post['title'])) {
            $data['title'] = $this->request->post['title'];
        } elseif (!empty($supplier_registration_info)) {
            $data['title'] = $supplier_registration_info[1]['title'];
        } else {
            $data['title'] = '';
        }

        if (isset($this->request->post['title-ar'])) {
            $data['title_ar'] = $this->request->post['title-ar'];
        } elseif (!empty($supplier_registration_info)) {
            $data['title_ar'] = $supplier_registration_info[2]['title'];
        } else {
            $data['title_ar'] = '';
        }

        if (isset($this->request->post['published'])) {
            $data['published'] = $this->request->post['published'];
        } elseif (!empty($supplier_registration_info)) {
            $data['published'] = $supplier_registration_info[1]['published_by'];
        } else {
            $data['published'] = '';
        }

        if (isset($this->request->post['published-ar'])) {
            $data['published_ar'] = $this->request->post['published-ar'];
        } elseif (!empty($supplier_registration_info)) {
            $data['published_ar'] = $supplier_registration_info[2]['published_by'];
        } else {
            $data['published_ar'] = '';
        }

        if (isset($this->request->post['activity'])) {
            $data['activity'] = $this->request->post['activity'];
        } elseif (!empty($supplier_registration_info)) {
            $data['activity'] = $supplier_registration_info[1]['activity_by'];
        } else {
            $data['activity'] = '';
        }

        if (isset($this->request->post['activity-ar'])) {
            $data['activity_ar'] = $this->request->post['activity-ar'];
        } elseif (!empty($supplier_registration_info)) {
            $data['activity_ar'] = $supplier_registration_info[2]['activity_by'];
        } else {
            $data['activity_ar'] = '';
        }

        if (isset($this->request->post['date'])) {
            $data['date'] = $this->request->post['date'];
        } elseif (!empty($supplier_registration_info)) {
            $data['date'] = $supplier_registration_info['date'];
        } else {
            $data['date'] = '';
        }


        // Image
        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($supplier_registration_info)) {
            $data['image'] = $supplier_registration_info['image'];
        } else {
            $data['image'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($supplier_registration_info) && is_file(DIR_IMAGE . $supplier_registration_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($supplier_registration_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);


        $data['user_token'] = $this->session->data['user_token'];

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/supplier_registration_form', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'catalog/supplier_registration')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['title']) < 1) || (utf8_strlen($this->request->post['title']) > 255)) {
            $this->error['title'] = $this->language->get('error_title');
        }

        if ((utf8_strlen($this->request->post['title-ar']) < 1) || (utf8_strlen($this->request->post['title-ar']) > 255)) {
            $this->error['title-ar'] = $this->language->get('error_title');
        }

        if ((utf8_strlen($this->request->post['published']) < 1) || (utf8_strlen($this->request->post['published']) > 255)) {
            $this->error['published'] = $this->language->get('error_published_by');
        }

        if ((utf8_strlen($this->request->post['published-ar']) < 1) || (utf8_strlen($this->request->post['published-ar']) > 255)) {
            $this->error['published-ar'] = $this->language->get('error_published_by');
        }

        if ((utf8_strlen($this->request->post['activity']) < 1) || (utf8_strlen($this->request->post['activity']) > 255)) {
            $this->error['activity'] = $this->language->get('error_activity_by');
        }

        if ((utf8_strlen($this->request->post['activity-ar']) < 1) || (utf8_strlen($this->request->post['activity-ar']) > 255)) {
            $this->error['activity-ar'] = $this->language->get('error_activity_by');
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'catalog/supplier_registration')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

}

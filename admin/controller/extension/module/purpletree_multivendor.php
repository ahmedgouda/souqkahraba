<?php
class ControllerExtensionModulePurpletreeMultivendor extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/purpletree_multivendor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			if($this->request->post['module_purpletree_multivendor_validate_text']==0 || !$this->config->get('module_purpletree_multivendor_status')){ 
				$module	    	= 'purpletree_multivendor_oc';

				if($_SERVER['HTTP_HOST'] == 'localhost') {
					$domain = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
				} else {
					$domain = 'http://'.$_SERVER['HTTP_HOST'];
				} 
				$valuee = $this->request->post['module_purpletree_multivendor_process_data'];
				 $ip_address = $this->get_client_ip();
				$url = "https://www.process.purpletreesoftware.com/occheckdata.php";

				$handle=curl_init($url);
				curl_setopt($handle, CURLOPT_VERBOSE, true);
				curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($handle, CURLOPT_POSTFIELDS,
							"process_data=$valuee&domain_name=$domain&ip_address=$ip_address&module_name=$module");
				$result = curl_exec($handle);
				$result1 = json_decode($result);

				if (true) {
					if (preg_match('(localhost|demo|test)',$domain)) {
						$str = 'qtriangle.in';
						$this->request->post['module_purpletree_multivendor_encypt_text'] = md5($str);
						$this->request->post['module_purpletree_multivendor_live_validate_text']=0;
					} else {
						$this->request->post['module_purpletree_multivendor_encypt_text'] = md5($domain);
						$this->request->post['module_purpletree_multivendor_live_validate_text']=1;
					}
					$this->request->post['module_purpletree_multivendor_validate_text']=1;
					$this->model_setting_setting->editSetting('module_purpletree_multivendor', $this->request->post);

					$this->session->data['success'] = $this->language->get('text_success');
				 } else {
					$this->session->data['warning'] = $this->language->get('text_license_error');
				} 
			} else {
				$this->model_setting_setting->editSetting('module_purpletree_multivendor', $this->request->post);

				$this->session->data['success'] = $this->language->get('text_success');
			}
			

			$this->response->redirect($this->url->link('extension/module/purpletree_multivendor', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['vendor_heading'] = $this->language->get('vendor_heading');
		$data['order_heading'] = $this->language->get('order_heading');
		$data['seller_product_heading'] = $this->language->get('seller_product_heading');
		$data['seller_review_heading'] = $this->language->get('seller_review_heading');
		$data['seller_email_heading'] = $this->language->get('seller_email_heading');
		$data['seller_store_heading'] = $this->language->get('seller_store_heading');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_allowed_categories'] = $this->language->get('text_allowed_categories');
		$data['text_selected_categories'] = $this->language->get('text_selected_categories');
		$data['text_assign_categories'] = $this->language->get('text_assign_categories');
		$data['text_store_email'] = $this->language->get('text_store_email');
		$data['text_store_phone'] = $this->language->get('text_store_phone');
		$data['text_store_address'] = $this->language->get('text_store_address');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_commission'] = $this->language->get('entry_commission');
		$data['entry_seller_manage_order'] = $this->language->get('entry_seller_manage_order');
		$data['entry_seller_approval'] = $this->language->get('entry_seller_approval');
		$data['entry_product_approval'] = $this->language->get('entry_product_approval');
		$data['entry_product_edit_approval'] = $this->language->get('entry_product_edit_approval');
		$data['entry_allow_category'] = $this->language->get('entry_allow_category');
		$data['entry_become_seller'] = $this->language->get('entry_become_seller');
		$data['entry_order_approval'] = $this->language->get('entry_order_approval');
		$data['entry_allow_related'] = $this->language->get('entry_allow_related');
		$data['entry_limit_purchase'] = $this->language->get('entry_limit_purchase');
		$data['entry_seller_review'] = $this->language->get('entry_seller_review');
		$data['entry_license'] = $this->language->get('entry_license');
		$data['entry_seller_store'] = $this->language->get('entry_seller_store');
		$data['entry_seller_invoice'] = $this->language->get('entry_seller_invoice');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} elseif(isset($this->session->data['warning'])){ 
			$data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['error_warning'] = '';
		}
		
		if(isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if (isset($this->error['commission'])) {
			$data['commission_error'] = $this->error['commission'];
		} 
		
		if (isset($this->error['product_limit'])) {
			$data['product_limit_error'] = $this->error['product_limit'];
		} 
		
		if (isset($this->error['process_data'])) {
			$data['process_data_error'] = $this->error['process_data'];
		} 

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/purpletree_multivendor', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/purpletree_multivendor', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_purpletree_multivendor_status'])) {
			$data['module_purpletree_multivendor_status'] = $this->request->post['module_purpletree_multivendor_status'];
		} else {
			$data['module_purpletree_multivendor_status'] = $this->config->get('module_purpletree_multivendor_status');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_process_data'])) {
			$data['module_purpletree_multivendor_process_data'] = $this->request->post['module_purpletree_multivendor_process_data'];
		} else {
			$data['module_purpletree_multivendor_process_data'] = $this->config->get('module_purpletree_multivendor_process_data');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_validate_text'])) {
			$data['module_purpletree_multivendor_validate_text'] = 1;
		} else {
			$data['module_purpletree_multivendor_validate_text'] = $this->config->get('module_purpletree_multivendor_validate_text');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_live_validate_text'])) {
			$data['module_purpletree_multivendor_live_validate_text'] = 0;
		} else {
			$data['module_purpletree_multivendor_live_validate_text'] = $this->config->get('module_purpletree_multivendor_live_validate_text');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_encypt_text'])) {
			$str = 'qtriangle.in';
			$data['module_purpletree_multivendor_encypt_text'] = md5($str);
		} else {
			$data['module_purpletree_multivendor_encypt_text'] = $this->config->get('module_purpletree_multivendor_encypt_text');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_commission'])) {
			$data['module_purpletree_multivendor_commission'] = $this->request->post['module_purpletree_multivendor_commission'];
		} else {
			$data['module_purpletree_multivendor_commission'] = $this->config->get('module_purpletree_multivendor_commission');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_seller_manage_order'])) {
			$data['module_purpletree_multivendor_seller_manage_order'] = $this->request->post['module_purpletree_multivendor_seller_manage_order'];
		} else {
			$data['module_purpletree_multivendor_seller_manage_order'] = $this->config->get('module_purpletree_multivendor_seller_manage_order');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_seller_approval'])) {
			$data['module_purpletree_multivendor_seller_approval'] = $this->request->post['module_purpletree_multivendor_seller_approval'];
		} else {
			$data['module_purpletree_multivendor_seller_approval'] = $this->config->get('module_purpletree_multivendor_seller_approval');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_product_approval'])) {
			$data['module_purpletree_multivendor_product_approval'] = $this->request->post['module_purpletree_multivendor_product_approval'];
		} else {
			$data['module_purpletree_multivendor_product_approval'] = $this->config->get('module_purpletree_multivendor_product_approval');
		}
		
		if (isset($this->request->post['module_module_purpletree_multivendor_allow_categorytype'])) {
			$data['module_module_purpletree_multivendor_allow_categorytype'] = $this->request->post['module_module_purpletree_multivendor_allow_categorytype'];
		} else {
			$data['module_module_purpletree_multivendor_allow_categorytype'] = $this->config->get('module_module_purpletree_multivendor_allow_categorytype');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_allow_category'])) {
			$data['module_purpletree_multivendor_allow_category'] = $this->request->post['module_purpletree_multivendor_allow_category'];
		} else {
			$data['module_purpletree_multivendor_allow_category'] = $this->config->get('module_purpletree_multivendor_allow_category');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_become_seller'])) {
			$data['module_purpletree_multivendor_become_seller'] = $this->request->post['module_purpletree_multivendor_become_seller'];
		} else {
			$data['module_purpletree_multivendor_become_seller'] = $this->config->get('module_purpletree_multivendor_become_seller');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_allow_related_product'])) {
			$data['module_purpletree_multivendor_allow_related_product'] = $this->request->post['module_purpletree_multivendor_allow_related_product'];
		} else {
			$data['module_purpletree_multivendor_allow_related_product'] = $this->config->get('module_purpletree_multivendor_allow_related_product');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_product_limit'])) {
			$data['module_purpletree_multivendor_product_limit'] = $this->request->post['module_purpletree_multivendor_product_limit'];
		} else {
			$data['module_purpletree_multivendor_product_limit'] = $this->config->get('module_purpletree_multivendor_product_limit');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_seller_review'])) {
			$data['module_purpletree_multivendor_seller_review'] = $this->request->post['module_purpletree_multivendor_seller_review'];
		} else {
			$data['module_purpletree_multivendor_seller_review'] = $this->config->get('module_purpletree_multivendor_seller_review');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_store_email'])) {
			$data['module_purpletree_multivendor_store_email'] = $this->request->post['module_purpletree_multivendor_store_email'];
		} else {
			$data['module_purpletree_multivendor_store_email'] = $this->config->get('module_purpletree_multivendor_store_email');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_store_phone'])) {
			$data['module_purpletree_multivendor_store_phone'] = $this->request->post['module_purpletree_multivendor_store_phone'];
		} else {
			$data['module_purpletree_multivendor_store_phone'] = $this->config->get('module_purpletree_multivendor_store_phone');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_store_address'])) {
			$data['module_purpletree_multivendor_store_address'] = $this->request->post['module_purpletree_multivendor_store_address'];
		} else {
			$data['module_purpletree_multivendor_store_address'] = $this->config->get('module_purpletree_multivendor_store_address');
		}
		
		if (isset($this->request->post['module_purpletree_multivendor_seller_invoice'])) {
			$data['module_purpletree_multivendor_seller_invoice'] = $this->request->post['module_purpletree_multivendor_seller_invoice'];
		} else {
			$data['module_purpletree_multivendor_seller_invoice'] = $this->config->get('module_purpletree_multivendor_seller_invoice');
		}
		
		$data['user_token'] = $this->session->data['user_token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/purpletree_multivendor', $data));
	}
	
	public function get_client_ip() {
		$ipaddress = '';
			if (getenv('HTTP_CLIENT_IP'))
				$ipaddress = getenv('HTTP_CLIENT_IP');
			else if(getenv('HTTP_X_FORWARDED_FOR'))
				$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
			else if(getenv('HTTP_X_FORWARDED'))
				$ipaddress = getenv('HTTP_X_FORWARDED');
			else if(getenv('HTTP_FORWARDED_FOR'))
				$ipaddress = getenv('HTTP_FORWARDED_FOR');
			else if(getenv('HTTP_FORWARDED'))
			   $ipaddress = getenv('HTTP_FORWARDED');
			else if(getenv('REMOTE_ADDR'))
				$ipaddress = getenv('REMOTE_ADDR');
			else
				$ipaddress = 'UNKNOWN';
			return $ipaddress;
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/purpletree_multivendor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
//		if($this->request->post['module_purpletree_multivendor_commission'] > 100){
//
//		$this->error['commission'] = $this->language->get('error_commission');
//
//
//		}elseif( ! filter_var($this->request->post['module_purpletree_multivendor_commission'], FILTER_VALIDATE_FLOAT) ){
//
//			$this->error['commission'] = $this->language->get('error_commission');
//
//		} elseif($this->request->post['module_purpletree_multivendor_commission'] < 0){
//			$this->error['commission'] = $this->language->get('error_commission');
//		}
		if(utf8_strlen($this->request->post['module_purpletree_multivendor_process_data']) < 1 ){
			$this->error['process_data'] = $this->language->get('error_process_data');
		}
		if($this->request->post['module_purpletree_multivendor_product_limit'] < 1 ){
			$this->error['product_limit'] = $this->language->get('error_product_limit');
		}
		return !$this->error;
	}
}
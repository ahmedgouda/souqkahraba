<?php
// Heading
$_['heading_title']         = 'Stores';

// Text
$_['text_success']          = 'Success: You have modified stores!';
$_['text_list']             = 'Store List';
$_['text_add']              = 'Add Store';
$_['text_edit']             = 'Edit Store';
$_['text_default']          = 'Default';
$_['text_balance']          = 'Balance';
$_['text_select']          = '--Please Select--';
$_['text_none']          = '--None--';
$_['text_enabled']          = 'Enabled';
$_['text_disabled']          = 'Disabled';
$_['text_yes']          = 'Yes';
$_['text_no']          = 'No';

$_['button_approve'] = 'Approve';
$_['button_disapprove'] = 'Disable';
$_['button_manage_products'] = 'Manage Products';
$_['button_manage_orders'] = 'Manage Orders';
$_['button_manage_payments'] = 'Payments';
$_['button_manage_commissions'] = 'Manage Commissions';
$_['button_manage_reviews'] = 'Manage Reviews';

// Column
$_['column_storename']           = 'Store Name';
$_['column_storeemail']          = 'Store E-Mail';
$_['column_storestatus']         = 'Store Status';
$_['column_storedate_added']     = 'Date Added';
$_['column_storeurl']        = 'Store Url';
$_['column_name']        = 'Seller Name';
$_['column_date_added']        = 'Date Added';
$_['column_action']        = 'Action';
$_['column_storephone']        = 'Store Phone';
$_['column_storeaddress']        = 'Store Address';
$_['column_approved']          = 'Is Approved';
$_['column_is_removed']          = 'Is Removed';


// Entry
$_['entry_storename']       = 'Store Name';
$_['entry_storeurl']       = 'Store Url';
$_['entry_storeemail']           = 'Store E-Mail';
$_['entry_storephone']       = 'Store Phone';
$_['entry_storelogo']             = 'Store Logo';
$_['entry_storebanner']             = 'Store Banner';
$_['entry_storebanner_desc']             = '(For best view banner size 900x300 px)';
$_['entry_storestatus']         			 = 'Store Status';
$_['entry_storeaddress']       = 'Store Address';
$_['entry_storecity']            = 'Store City';
$_['entry_storepostcode']        = 'Store Postcode';
$_['entry_storecountry']         = 'Store Country';
$_['entry_storezone']            = 'Store Region / State';
$_['entry_storedescription']     = 'Store Description';
$_['entry_storeshippingpolicy']     = 'Store Shipping Policy';
$_['entry_storereturn']     = 'Store Return Policy';
$_['entry_storemetakeyword']          = 'Store Meta Keywords';
$_['entry_storemetadescription']          = 'Store Meta Description';
$_['entry_storebankdetail']          = 'Store Bank Detail';
$_['entry_storetin']          = 'Store TIN <br>(Tax Identification number)';
$_['entry_name']            = 'Seller Name';
$_['entry_date_added']      = 'Date Added';
$_['entry_storestatus']      = 'Store Status';
$_['entry_storeshipping']      = 'Store Shipping Charge';
$_['entry_removed']      = 'Removed';
$_['entry_storeseo']      = 'SEO URL For Store';
$_['entry_sellercommission']      = 'Seller commission in percentage';

$_['tab_storedetail']      = 'Store Details';
$_['tab_productlisting']      = 'Seller Products';
$_['tab_product_assign']      = 'Add Product';
$_['tab_seller_orders']      = 'Seller Orders';
$_['tab_storeshipping']      = 'Shipping Charge';
$_['tab_seller_payment']      = 'Payment settlement';
$_['tab_seller_commission']      = 'Seller commission';
$_['tab_seller_review']      = 'Reviews';

// Help

// Error
$_['error_warning']         = 'Warning: Please check the form carefully for errors!';
$_['error_permission']      = 'Warning: You do not have permission to modify customers!';
$_['error_exists']          = 'Warning: E-Mail Address is already registered!';
$_['error_storename']       = 'Store name must be between 1 and 32 characters!';
$_['error_storeurl']        = 'Store url must be between 1 and 32 characters!';
$_['error_storeemail']           = 'E-Mail Address does not appear to be valid!';
$_['error_storephone']       = 'Enter valid phone no.!';
$_['error_storedescription']       = 'Description must be between 3 and 32 characters!';
$_['error_storeaddress']       = 'Address  must be between 3 and 128 characters!';
$_['error_storeshipping']       = 'Shipping policy  must be between 3 and 128 characters!';
$_['error_storereturn']       = 'Return policy  must be between 3 and 128 characters!';
$_['error_storemetakeywords']       = 'Meta keywords required!';
$_['error_storemetadescription']       = 'Meta description required!';
$_['error_storebankdetail']       = 'Bank details required!';
$_['error_storetin']       = 'TIN required!';
$_['error_storecity']            = 'City must be between 2 and 128 characters!';
$_['error_storepostcode']        = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_storecountry']         = 'Please select a country!';
$_['error_storezone']            = '%s required!';

$_['error_sellertransaction']            = 'Transaction Id is required!';
$_['error_selleramount']            = 'Enter correct amount!';
$_['error_sellerpaymode']            = 'Enter correct payment mode!';
$_['error_paymentstatus']            = 'Payment status is required';
$_['error_storeshippingcharge']            = 'Enter valid shipping charge!';
$_['error_storeseo'] = 'SEO url already exist. Please try another.';
$_['error_storeseoempty'] = 'This field is required.';
$_['error_commission'] = 'Please enter valid commission!';
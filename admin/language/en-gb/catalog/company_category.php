<?php
// Heading
$_['heading_title']          = 'Companies\' Categories';



// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Category Name must be between 1 and 255 characters!';

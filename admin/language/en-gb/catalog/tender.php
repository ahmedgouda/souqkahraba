<?php
// Heading
$_['heading_title']          = 'Tenders';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_title']             = 'Title must be between 1 and 255 characters!';
$_['error_published_by']             = 'Published by must be between 1 and 255 characters!';
$_['error_activity_by']             = 'Activity by must be between 1 and 255 characters!';
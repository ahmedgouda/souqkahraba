<?php
// Heading
$_['heading_title']    = 'Purpletree Shipping';

// Text
$_['text_extension']   = 'Extensions';
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified purpletree shipping!';
$_['text_edit']        = 'Edit Purpletree Shipping';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify purpletree shipping!';
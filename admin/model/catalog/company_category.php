<?php
class ModelCatalogCompanyCategory extends Model {
    public function addCategory($data) {
        $this->db->query("INSERT INTO oc_company_category SET company_category_name = '" . $data['name'] . "', language_id = '1'");
        $category_id = $this->db->getLastId();

        $this->db->query("INSERT INTO oc_company_category SET company_category_name = '" . $data['name-ar'] . "', language_id = '2', id_company_category = '" . $category_id . "'");

        $this->cache->delete('company_category');

        return $category_id;
    }

    public function editCategory($category_id, $data) {
        $this->db->query("UPDATE oc_company_category SET company_category_name = '" . $data['name'] . "' WHERE id_company_category = '" . (int)$category_id . "' AND language_id ='1'");

        $this->db->query("UPDATE oc_company_category SET company_category_name = '" . $data['name-ar'] . "' WHERE id_company_category = '" . (int)$category_id . "' AND language_id ='2'");

        $this->cache->delete('company_category');
    }

    public function deleteCategory($category_id) {
        $this->db->query("DELETE FROM oc_company_category WHERE id_company_category = '" . (int)$category_id . "'");

        $this->cache->delete('company_category');
    }

    public function getCategory($category_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM oc_company_category WHERE id_company_category = '" . (int)$category_id . "'");

        $categories = [];

        foreach ($query->rows as $row) {
            $categories[(int)$row['language_id']] = $row['company_category_name'];
        }
        return $categories;
    }

    public function getCategories() {
        $sql = "SELECT * FROM oc_company_category WHERE language_id ='1'";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalCompaniesByCategoryId($category_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "purpletree_vendor_stores WHERE category_id = '" . (int)$category_id . "'");

        return $query->row['total'];
    }
}
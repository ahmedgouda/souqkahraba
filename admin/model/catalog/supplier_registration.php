<?php
class ModelCatalogSupplierRegistration extends Model {
    public function getSupplierRegistrations() {
        $sql = "SELECT * FROM oc_supplier_registration WHERE language_id ='1' ORDER BY supplier_registration_id DESC";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function addSupplierRegistration($data) {
        $this->db->query("INSERT INTO oc_supplier_registration SET title = '" . $data['title'] . "', date = '" . $data['date'] . "', published_by = '" . $data['published'] . "', activity_by = '" . $data['activity'] . "', image = '" . $data['image'] . "', language_id = '1'");
        $supplier_registration_id = $this->db->getLastId();

        $this->db->query("INSERT INTO oc_supplier_registration SET supplier_registration_id = '" . $supplier_registration_id . "', title = '" . $data['title-ar'] . "', date = '" . $data['date'] . "', published_by = '" . $data['published-ar'] . "', activity_by = '" . $data['activity-ar'] . "', image = '" . $data['image'] . "', language_id = '2'");

        $this->cache->delete('supplier_registration');

        return $supplier_registration_id;
    }

    public function editSupplierRegistration($supplier_registration_id, $data) {
        $this->db->query("UPDATE oc_supplier_registration SET title = '" . $data['title'] . "', date = '" . $data['date'] . "', published_by = '" . $data['published'] . "', activity_by = '" . $data['activity'] . "', image = '" . $data['image'] . "' WHERE supplier_registration_id = '" . (int)$supplier_registration_id . "' AND language_id ='1'");

        $this->db->query("UPDATE oc_supplier_registration SET title = '" . $data['title-ar'] . "', date = '" . $data['date'] . "', published_by = '" . $data['published-ar'] . "', activity_by = '" . $data['activity-ar'] . "', image = '" . $data['image'] . "' WHERE supplier_registration_id = '" . (int)$supplier_registration_id . "' AND language_id ='2'");

        $this->cache->delete('supplier_registration');
    }

    public function deleteSupplierRegistration($supplier_registration_id) {
        $this->db->query("DELETE FROM oc_supplier_registration WHERE supplier_registration_id = '" . (int)$supplier_registration_id . "'");

        $this->cache->delete('supplier_registration');
    }

    public function getSupplierRegistration($supplier_registration_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM oc_supplier_registration WHERE supplier_registration_id = '" . (int)$supplier_registration_id . "'");

        $supplier_registrations = [];

        foreach ($query->rows as $row) {
            $supplier_registration = [];
            $supplier_registration['title'] = $row['title'];
            $supplier_registration['published_by'] = $row['published_by'];
            $supplier_registration['activity_by'] = $row['activity_by'];
            $supplier_registrations[(int)$row['language_id']] = $supplier_registration;
            $supplier_registrations['date'] = $row['date'];
            $supplier_registrations['image'] = $row['image'];
        }

        return $supplier_registrations;
    }
}
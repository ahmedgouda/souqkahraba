<?php
class ModelCatalogTender extends Model {
    public function getTenders() {
        $sql = "SELECT * FROM oc_tender WHERE language_id ='1' ORDER BY tender_id DESC";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function addTender($data) {
        $this->db->query("INSERT INTO oc_tender SET title = '" . $data['title'] . "', date = '" . $data['date'] . "', published_by = '" . $data['published'] . "', activity_by = '" . $data['activity'] . "', image = '" . $data['image'] . "', language_id = '1'");
        $tender_id = $this->db->getLastId();

        $this->db->query("INSERT INTO oc_tender SET tender_id = '" . $tender_id . "', title = '" . $data['title-ar'] . "', date = '" . $data['date'] . "', published_by = '" . $data['published-ar'] . "', activity_by = '" . $data['activity-ar'] . "', image = '" . $data['image'] . "', language_id = '2'");

        $this->cache->delete('tender');

        return $tender_id;
    }

    public function editTender($tender_id, $data) {
        $this->db->query("UPDATE oc_tender SET title = '" . $data['title'] . "', date = '" . $data['date'] . "', published_by = '" . $data['published'] . "', activity_by = '" . $data['activity'] . "', image = '" . $data['image'] . "' WHERE tender_id = '" . (int)$tender_id . "' AND language_id ='1'");

        $this->db->query("UPDATE oc_tender SET title = '" . $data['title-ar'] . "', date = '" . $data['date'] . "', published_by = '" . $data['published-ar'] . "', activity_by = '" . $data['activity-ar'] . "', image = '" . $data['image'] . "' WHERE tender_id = '" . (int)$tender_id . "' AND language_id ='2'");

        $this->cache->delete('tender');
    }

    public function deleteTender($tender_id) {
        $this->db->query("DELETE FROM oc_tender WHERE tender_id = '" . (int)$tender_id . "'");

        $this->cache->delete('tender');
    }

    public function getTender($tender_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM oc_tender WHERE tender_id = '" . (int)$tender_id . "'");

        $tenders = [];

        foreach ($query->rows as $row) {
            $tender = [];
            $tender['title'] = $row['title'];
            $tender['published_by'] = $row['published_by'];
            $tender['activity_by'] = $row['activity_by'];
            $tenders[(int)$row['language_id']] = $tender;
            $tenders['date'] = $row['date'];
            $tenders['image'] = $row['image'];
        }

        return $tenders;
    }
}
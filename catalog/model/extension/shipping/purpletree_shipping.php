<?php
class ModelExtensionShippingPurpletreeShipping extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/purpletree_shipping');
		$method_data = array();
		$quote_data = array();

		$quote_data['purpletree_shipping'] = array(
			'code'         => 'purpletree_shipping.purpletree_shipping',
			'title'        => $this->language->get('text_title'),
			'cost'         => $this->cart->getSellerShippingCharge(),
			'tax_class_id' => 0,
			'text'         => $this->currency->format($this->cart->getSellerShippingCharge(), $this->session->data['currency'])
		);
			
		$method_data = array(
			'code'       => 'purpletree_shipping',
			'title'      => $this->language->get('text_title'),
			'quote'      => $quote_data,
			'sort_order' => $this->config->get('purpletree_shipping_sort_order'),
			'error'      => false
		);

		return $method_data;
	}
}
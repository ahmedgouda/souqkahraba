<?php 
class ModelExtensionPurpletreeMultivendorSellercontact extends Model{
	
	public function getSellerContact($data=array()){
		
		if ($data['start'] < 0) {
			$data['start'] = 0;
		}

		if ($data['limit'] < 1) {
			$data['limit'] = 1;
		}
		
		$sql = "SELECT * FROM " . DB_PREFIX . "purpletree_vendor_contact WHERE seller_id = '".$data['seller_id']."'";
		
		$sql .=" ORDER BY id DESC LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getTotalSellerContact($data=array()){
	
		$sql = "SELECT count(*) AS total FROM " . DB_PREFIX . "purpletree_vendor_contact WHERE seller_id = '".$data['seller_id']."'";
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function addContact($data){
		$this->db->query("INSERT into " . DB_PREFIX . "purpletree_vendor_contact SET seller_id= '".(int)$data['seller_id']."', customer_name ='".$data['customer_name']."', telephone='" . $this->db->escape($data['telephone']) . "', customer_email='".$this->db->escape($data['customer_email'])."', customer_message='".$this->db->escape($data['customer_message'])."', created_at=NOW(), updated_at=NOW()");
	}
	
	public function getStoreEmail($id){
		$seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purpletree_vendor_stores WHERE seller_id = '" . (int)$id . "'");

		if ($seller_query->row['store_email']) {
			$email = $seller_query->row['store_email'];
		} else {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$id . "'");
			$email = $query->row['email'];	
		}
		return $email;		
	}

}
?>
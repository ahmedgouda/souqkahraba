<?php 
class ModelExtensionPurpletreeMultivendorVendor extends Model{
	public function addSeller($customer_id,$store_name){
		$this->db->query("INSERT into " . DB_PREFIX . "purpletree_vendor_stores SET seller_id ='".$customer_id."', store_name='".$store_name."', store_status='".(int)(!$this->config->get('module_purpletree_multivendor_seller_approval'))."', store_created_at= NOW(), store_updated_at= NOW()");
		$store_id = $this->db->getLastId();
	}
	
	public function becomeSeller($customer_id,$store_name){
		if($store_name['become_seller']){
			$this->db->query("INSERT into " . DB_PREFIX . "purpletree_vendor_stores SET seller_id ='".$customer_id."', store_name='".$store_name['seller_storename']."', store_status='".(int)(!$this->config->get('module_purpletree_multivendor_seller_approval'))."', store_created_at= NOW(), store_updated_at= NOW()");
			$store_id = $this->db->getLastId();
		}
		else {
			$store_id = 0;
		}
		return $store_id;
		
	}
	
	public function reseller($customer_id,$store_name){
		if($store_name['become_seller']){	
			$this->db->query("UPDATE " . DB_PREFIX . "purpletree_vendor_stores SET store_status='".(int)(!$this->config->get('module_purpletree_multivendor_seller_approval'))."', is_removed=0 WHERE seller_id='".$customer_id."'");
			$store_id = 1;
		}
		else {
			$store_id = 0;
		}
		return $store_id;
		
	}
	
	public function getSellerStorename($store_name){
		$query = $this->db->query("SELECT id FROM " . DB_PREFIX . "purpletree_vendor_stores where store_name='".$store_name."'");
		return $query->num_rows;
	}
	
	public function getStoreRating($seller_id){
		$query = $this->db->query("SELECT AVG(rating) as rating,count(*) as count FROM " . DB_PREFIX . "purpletree_vendor_reviews where seller_id='".$seller_id."' AND status=1");
		return $query->row;
	}
	
	public function getStore($store_id){
		$query = $this->db->query("SELECT pvs.*,CONCAT(c.firstname, ' ', c.lastname) AS seller_name, (SELECT website FROM " . DB_PREFIX . "customer_affiliate WHERE customer_id = pvs.seller_id LIMIT 1) As website, (SELECT keyword FROM " . DB_PREFIX . "seo_url WHERE query = 'seller_store_id=" . (int)$store_id . "') AS store_seo FROM " . DB_PREFIX . "purpletree_vendor_stores pvs JOIN " . DB_PREFIX . "customer c ON(c.customer_id = pvs.seller_id) where pvs.id='".$store_id."'");
		return $query->row;
	}
	
	public function getStoreArabic($store_id) {
		$query = $this->db->query("SELECT * FROM oc_purpletree_store_arabic WHERE store_id = " .(int)$store_id . " LIMIT 1;");
		return $query->row;
	}
	
	public function getStoreDetail($customer_id){
		$query = $this->db->query("SELECT pvs.* FROM " . DB_PREFIX . "purpletree_vendor_stores pvs where pvs.seller_id='".$customer_id."'");
		return $query->row;
	}
	

	public function editStore($store_id,$data){
		
		$this->db->query("UPDATE " . DB_PREFIX. "purpletree_vendor_stores SET store_name='".$this->db->escape($data['store_name'])."', store_logo='".$this->db->escape($data['store_logo'])."', store_email='".$this->db->escape($data['store_email'])."', store_phone='".$this->db->escape($data['store_phone'])."', store_banner='".$this->db->escape($data['store_banner'])."', store_description='".$this->db->escape($data['store_description'])."', store_address='".$this->db->escape($data['store_address'])."', store_city='".$this->db->escape($data['store_city'])."',store_country='".(int)$data['store_country']."', store_state='".(int)$data['store_state']."', store_zipcode='".$data['store_zipcode']."', store_shipping_policy='".$this->db->escape($data['store_shipping_policy'])."', store_return_policy='".$this->db->escape($data['store_return_policy'])."', store_meta_keywords='".$this->db->escape($data['store_meta_keywords'])."', store_meta_descriptions='".$this->db->escape($data['store_meta_description'])."', store_bank_details='".$this->db->escape($data['store_bank_details'])."', store_tin='".$this->db->escape($data['store_tin'])."', store_shipping_charge ='".$this->db->escape($data['store_shipping_charge'])."',store_updated_at=NOW() where id='".$store_id."'");
		
		if ($data['store_seo']) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE query = 'seller_store_id=" . (int)$store_id . "'");
			if($query->num_rows > 0){
				$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE query = 'seller_store_id=" . (int)$store_id . "'");
				
				$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET query = 'seller_store_id=" . (int)$store_id . "', keyword = '" . $this->db->escape($data['store_seo']) . "'");
			} else{
				$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET query = 'seller_store_id=" . (int)$store_id . "', keyword = '" . $this->db->escape($data['store_seo']) . "'");
			}
		}
	}
	public function getStoreByEmail($email) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purpletree_vendor_stores WHERE LCASE(store_email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
		
	}
	
	public function getStoreSeo($seo_url) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE LCASE(keyword) = '".$this->db->escape(utf8_strtolower($seo_url)) . "'");
		return $query->row;
	}
	
	public function removeSeller($seller_id){
		$this->db->query("UPDATE " . DB_PREFIX . "purpletree_vendor_products pvp JOIN " . DB_PREFIX . "product p ON(p.product_id=pvp.product_id) SET p.status=0 WHERE pvp.seller_id='".$seller_id."'");
		
		$this->db->query("UPDATE " . DB_PREFIX . "purpletree_vendor_stores SET store_status=0, is_removed=1 WHERE seller_id='".$seller_id."'");
	}
}
?>
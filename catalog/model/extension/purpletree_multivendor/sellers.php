<?php
class ModelExtensionPurpletreeMultivendorSellers extends Model{
	
	public function getSellers($data= array()){
		$sql = "SELECT pvs.*,(SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = pvs.seller_id) AS seller,(SELECT co.name FROM " . DB_PREFIX . "country co WHERE co.country_id = pvs.store_country) AS seller_country FROM " . DB_PREFIX . "purpletree_vendor_stores pvs WHERE pvs.store_status='1'";
		
		if(!empty($data['filter'])){
			$sql .=" HAVING seller LIKE '" . $this->db->escape($data['filter']) . "%'";
		}
		
		if (isset($data['sort'])) {
			$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
		}  else {
			$sql .= " ORDER BY " . $data['sort'];
		}
		
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getTotalSellers($data= array()){
		
		$sql = "SELECT (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = pvs.seller_id) AS seller FROM " . DB_PREFIX . "purpletree_vendor_stores pvs WHERE pvs.store_status='1'";
		if(!empty($data['filter'])){
			$sql .=" HAVING seller LIKE '" . $this->db->escape($data['filter']) . "%'";
		}
		$query = $this->db->query($sql);
		
		$query->row['total'] = $query->num_rows;
		
		return $query->row['total'];
	}
	
	public function getStoreArabic($store_id) {
		$query = $this->db->query("SELECT * FROM oc_purpletree_store_arabic WHERE store_id = " .(int)$store_id . " LIMIT 1;");
		return $query->row;
	}

    public function getCompanySellers($data= array()){
        $sql = "SELECT distinct pvs.*, (SELECT z.name FROM " . DB_PREFIX . "zone z WHERE z.zone_id = pvs.store_state) AS seller_state FROM " . DB_PREFIX . "purpletree_vendor_stores pvs, oc_purpletree_store_arabic psa WHERE pvs.store_status='1'";

        if(!empty($data['filter_location'])){
            $sql .=" AND pvs.store_state = '" . (int)$data['filter_location'] . "'";
        }

        if(!empty($data['filter_category'])){
            $sql .=" AND pvs.category_id = '" . (int)$data['filter_category'] . "'";
        }

        if(!empty($data['filter_name']) && strlen($data['filter_name'])> 0){
            $sql .=" AND (pvs.store_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
         
            $sql .=" OR(psa.store_id = pvs.id and psa.store_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'))";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalCompanySellers($data= array()){

        $sql = "SELECT distinct pvs.* FROM " . DB_PREFIX . "purpletree_vendor_stores pvs, oc_purpletree_store_arabic psa WHERE pvs.store_status='1'";

        if(!empty($data['filter_location'])){
            $sql .=" AND pvs.store_state = '" . (int)$data['filter_location'] . "'";
        }

        if(!empty($data['filter_category'])){
            $sql .=" AND pvs.category_id = '" . (int)$data['filter_category'] . "'";
        }

        if(!empty($data['filter_name']) && strlen($data['filter_name'])> 0){
            $sql .=" AND (pvs.store_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
            
            $sql .=" OR(psa.store_id = pvs.id and psa.store_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'))";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        $query->row['total'] = $query->num_rows;

        return $query->row['total'];
    }
	
	public function getTotalProducts($data= array()){
		
		$sql = "SELECT COUNT(pvp.id) AS total FROM " . DB_PREFIX . "purpletree_vendor_products pvp JOIN " . DB_PREFIX . "product p ON(p.product_id=pvp.product_id) WHERE pvp.is_approved='1' AND p.status ='1'";
		
		if(!empty($data['seller_id'])){
			$sql .= " AND pvp.seller_id ='".(int)$data['seller_id']."'";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getProducts($data= array()){
		
		$sql = "SELECT p.image, p.product_id FROM " . DB_PREFIX . "purpletree_vendor_products pvp JOIN " . DB_PREFIX . "product p ON(p.product_id=pvp.product_id) WHERE pvp.is_approved='1' AND p.status ='1'";
		
		if(!empty($data['seller_id'])){
			$sql .= " AND pvp.seller_id ='".(int)$data['seller_id']."'";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
}
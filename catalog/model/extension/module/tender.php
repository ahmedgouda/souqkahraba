<?php
class ModelExtensionModuleTender extends Model {
    public function getTenders($data= array()) {
        $sql = "SELECT * FROM oc_tender WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY tender_id DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalTenders($data= array()){

        $sql = "SELECT * FROM oc_tender WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        $query->row['total'] = $query->num_rows;

        return $query->row['total'];
    }

    public function getTender($tender_id) {
        $sql = "SELECT * FROM oc_tender WHERE tender_id = '" . $tender_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $query = $this->db->query($sql);

        return $query->row;
    }
}
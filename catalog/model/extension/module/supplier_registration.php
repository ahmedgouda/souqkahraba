<?php
class ModelExtensionModuleSupplierRegistration extends Model {
    public function getSupplierRegistrations($data= array()) {
        $sql = "SELECT * FROM oc_supplier_registration WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY supplier_registration_id DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalSupplierRegistrations($data= array()){

        $sql = "SELECT * FROM oc_supplier_registration WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        $query->row['total'] = $query->num_rows;

        return $query->row['total'];
    }

    public function getSupplierRegistration($supplier_registration_id) {
        $sql = "SELECT * FROM oc_supplier_registration WHERE supplier_registration_id = '" . $supplier_registration_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $query = $this->db->query($sql);

        return $query->row;
    }
}
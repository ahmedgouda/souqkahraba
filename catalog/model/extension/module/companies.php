<?php
class ModelExtensionModuleCompanies extends Model {
    public function getCategories() {
        $sql = "SELECT * FROM oc_company_category WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $query = $this->db->query($sql);

        $categories = [];
        $categories[0] = '';
        foreach ($query->rows as $category) {
            $categories[(int)$category['id_company_category']] = $category['company_category_name'];
        }

        return $categories;
    }
}
<?php 
$_['heading_title']   = 'Seller Panel';

$_['text_sellerstore']   = 'Store Information';
$_['text_sellerproduct']   = 'Manage Products';
$_['text_sellerprofile']   = 'Seller Profile';
$_['text_sellerorder']   = 'Orders';
$_['text_sellercommission']   = 'Commission';
$_['text_removeseller']   = 'Remove as a seller';
$_['text_becomeseller']   = 'Become a seller';
$_['text_sellerview']   = 'View Store';
$_['text_approval']   = '<b>Waiting for seller approval</b>';
$_['text_sellerpayment']   = 'Payments';
$_['text_sellerreview']   = 'My Reviews';
$_['text_sellerenquiry']   = 'Customer Enquiries';
?>
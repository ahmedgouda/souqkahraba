<?php
// Heading
$_['heading_title']         = 'الشركة';

//text

$_['text_dashboard']		= 'Dashboard';
$_['text_storeview']		= 'الشركة';
$_['text_returnpolicy']		= 'سياسة الارجاع';
$_['text_shippingpolicy']		= 'سياسة التوصيل';
$_['text_empty']        = "لا يوجد اي منتجات لهذة الشركة ليتم عرضها";
$_['text_error']        = "لم يتم العثور على الشركة";
$_['text_quantity']     = 'الكمية:';
$_['text_manufacturer'] = 'الماركة:';
$_['text_model']        = 'كود المنتج:';
$_['text_points']       = 'نقاط الجوائز:';
$_['text_price']        = 'السعر:';
$_['text_tax']          = 'الضرائب:';
$_['text_compare']      = 'مقارنة المنتج (%s)';
$_['text_sort']         = 'رتب حسب:';
$_['text_default']      = 'افتراضى';
$_['text_name_asc']     = 'الاسم(أ - ى)';
$_['text_name_desc']    = 'الاسم(ى - أ)';
$_['text_price_asc']    = 'السعر(الاقل &gt; الاعلى)';
$_['text_price_desc']   = 'السعر(الاعلى &gt; الاقل)';
$_['text_rating_asc']   = 'التقييم(الادنى)';
$_['text_rating_desc']  = 'التقيم(الاعلى)';
$_['text_model_asc']    = 'الموديل(أ - ى)';
$_['text_model_desc']   = 'الموديل(ى - أ)';
$_['text_limit']        = 'اعرض:';
$_['text_aboutstore']        = 'عن الشركة';
$_['text_sellerreview']        = 'التقييمات';
$_['text_sellercontact']		= 'راسل الشركة';

$_['text_remove_account_success'] = 'تم حذف حسابك بنجاح.!';

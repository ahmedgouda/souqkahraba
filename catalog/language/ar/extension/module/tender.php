<?php
// Heading
$_['heading_title']    = 'المناقصات';

// Text
$_['text_tender']    = 'مناقصة';
$_['text_tender_title']    = 'المناقصة : ';
$_['text_publish_by']    = 'نشر بواسطة : ';
$_['text_publish_date']    = 'تاريخ النشر : ';
$_['text_activity_by']    = 'صاحب النشاط : ';
$_['text_description']    = 'التفاصيل : ';
$_['text_back']    = 'الرجوع للمناقصات';

$_['text_empty']    = 'لا يوجد مناقصات';
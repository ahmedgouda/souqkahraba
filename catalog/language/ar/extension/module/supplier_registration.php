<?php
// Heading
$_['heading_title']    = 'قيد الموردين';

// Text
$_['text_supplier_registration']    = 'قيد موردين';
$_['text_supplier_registration_title']    = 'عنوان القيد : ';
$_['text_publish_by']    = 'نشر بواسطة : ';
$_['text_publish_date']    = 'تاريخ النشر : ';
$_['text_activity_by']    = 'صاحب النشاط : ';
$_['text_description']    = 'التفاصيل : ';
$_['text_back']    = 'الرجوع للقائمة';

$_['text_empty']    = 'لا يوجد قيد موردين';

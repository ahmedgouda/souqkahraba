<?php
// Heading
$_['heading_title']    = 'الشركات';

// Text
$_['text_search']    = 'بحث الشركات';
$_['text_name']    = 'اسم الشركة';
$_['text_location']    = 'المحافظة';
$_['text_category']    = 'الفئة';
$_['text_phone']    = 'رقم التليفون';
$_['text_email']    = 'البريد الالكترونى';
$_['button_search']    = 'بحث';
$_['text_empty']    = 'لا يوجد اى شركات';
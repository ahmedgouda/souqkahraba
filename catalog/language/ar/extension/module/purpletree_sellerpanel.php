<?php 
$_['heading_title']   = 'صفحة الشركة';

$_['text_sellerstore']   = 'معلومات الشركة';
$_['text_sellerproduct']   = 'تحكم فى المنتجات';
$_['text_sellerdownload']   = 'تحكم فى التحميلات';
$_['text_sellerprofile']   = 'ملف البائع';
$_['text_sellerorder']   = 'الطلبات';
$_['text_sellercommission']   = 'Commission';
$_['text_removeseller']   = 'تخلص من الشركة';
$_['text_becomeseller']   = 'كون شركة';
$_['text_sellerview']   = 'شاهد المحل';
$_['text_approval']   = '<b>فى انتظار الموافقة</b>';
$_['text_sellerpayment']   = 'الدفع';
$_['text_sellerreview']   = 'تقييماتى';
$_['text_sellerenquiry']   = 'تساؤلات الزبائن';
?>
<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Text
$_['text_title'] = 'الشحن';
$_['text_weight'] = 'مصاريف الشحن - بائع واحد';
$_['text_weight_multi_seller'] = 'مصاريف الشحن - اكثر من بائع';
<?php

// Heading
$_['heading_title']     = 'بائعون اخرون';

// Text
$_['text_empty']        = 'لا يوجد بائعون اخرونً.';
$_['text_price']        = 'السعر:';
$_['text_tax']          = 'السعر بدون ضريبة :';
$_['text_compare']      = 'مقارنة المنتج (%s)';
$_['text_limit']        = 'عرض:';

$_['text_seller_name']        = 'أسم المحل';
$_['text_seller_price']        = 'السعر';
$_['text_product_info']        = 'بيانات المنتج';
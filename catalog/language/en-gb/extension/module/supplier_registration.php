<?php
// Heading
$_['heading_title']    = 'Supplier registrations';

// Text
$_['text_supplier_registration']    = 'Supplier registration';
$_['text_supplier_registration_title']    = 'Supplier registration title : ';
$_['text_publish_by']    = 'Published Via : ';
$_['text_publish_date']    = 'Publish date : ';
$_['text_activity_by']    = 'Activity by : ';
$_['text_description']    = 'Description : ';
$_['text_back']    = 'Back to Supplier registrations';


$_['text_empty']    = 'No supplier registrations to list';


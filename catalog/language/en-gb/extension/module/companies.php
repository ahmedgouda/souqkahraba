<?php
// Heading
$_['heading_title']    = 'Companies';

// Text
$_['text_search']    = 'Companies search';
$_['text_name']    = 'Company Name';
$_['text_location']    = 'Location';
$_['text_category']    = 'Category';
$_['text_phone']    = 'Phone';
$_['text_email']    = 'Email';
$_['button_search']    = 'Search';
$_['text_empty']    = 'No companies found';
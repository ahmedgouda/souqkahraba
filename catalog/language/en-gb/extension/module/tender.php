<?php
// Heading
$_['heading_title']    = 'Tenders';

// Text
$_['text_tender']    = 'Tender';
$_['text_tender_title']    = 'Tender title : ';
$_['text_publish_by']    = 'Published Via : ';
$_['text_publish_date']    = 'Publish date : ';
$_['text_activity_by']    = 'Activity by : ';
$_['text_description']    = 'Description : ';
$_['text_back']    = 'Back to tenders';

$_['text_empty']    = 'No tenders to list';
<?php
class ControllerExtensionModuleCompanies extends Controller {
    public function index() {
        $this->load->language('extension/module/companies');

        $this->load->model('extension/module/companies');
        $this->load->model('extension/purpletree_multivendor/sellers');
        $this->load->model('localisation/zone');
        $this->load->model('tool/image');

        if (isset($this->request->get['search_text'])) {
            $filter_name = $this->request->get['search_text'];
        } else {
            $filter_name = '';
        }

        if (isset($this->request->get['search_location'])) {
            $filter_location = $this->request->get['search_location'];
        } else {
            $filter_location = '';
        }

        if (isset($this->request->get['search_category'])) {
            $filter_category = $this->request->get['search_category'];
        } else {
            $filter_category = '';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/companies', '')
        );

        $data['text_heading'] = $this->language->get('heading_title');
        $data['text_search'] = $this->language->get('text_search');
        $data['text_name'] = $this->language->get('text_name');
        $data['text_location'] = $this->language->get('text_location');
        $data['text_category'] = $this->language->get('text_category');
        $data['text_phone'] = $this->language->get('text_phone');
        $data['text_email'] = $this->language->get('text_email');
        $data['button_search'] = $this->language->get('button_search');
        $data['text_empty'] = $this->language->get('text_empty');

        $data['language'] = $this->config->get('config_language_id');

        $this->document->setTitle($this->language->get('heading_title'));
        $data['heading_title'] = $this->language->get('heading_title');

        $data['categories'] = $this->model_extension_module_companies->getCategories();
        $data['zones'] = $this->model_localisation_zone->getAllZones();

        $limit = '20';

        $filter_data_seller = array(
            'filter_name'              => $filter_name,
            'filter_location'              => $filter_location,
            'filter_category'              => $filter_category,
            'start'              => ($page - 1) * $limit,
            'limit'              => $limit
        );

		$filter_data_seller_total = array(
			'filter_name'              => $filter_name,
			'filter_location'              => $filter_location,
			'filter_category'              => $filter_category
		);

        $seller_totals = $this->model_extension_purpletree_multivendor_sellers->getTotalCompanySellers($filter_data_seller_total);

        $seller_lists = $this->model_extension_purpletree_multivendor_sellers->getCompanySellers($filter_data_seller_total);

        $data['sellers'] = array();

        foreach ($seller_lists as $seller_list) {
            if ($seller_list['store_logo']) {
                $data['seller_thumb'] = $this->model_tool_image->resize($seller_list['store_logo'],200 ,100 );
            } else {
                $data['seller_thumb'] = $this->model_tool_image->resize('placeholder.png', 200,100);
            }
            
            $seller_name = '';
            if($this->config->get('config_language_id') == 2) {
				$store_Arabic = $this->model_extension_purpletree_multivendor_sellers->getStoreArabic($seller_list['id']);
				if(!empty($store_Arabic)) {
					$seller_name = $store_Arabic['store_name'];
				}
				
			} else {
				$seller_name = $seller_list['store_name'];
			}

            $data['sellers'][] = array(
                'seller_thumb' => $data['seller_thumb'],
                'seller_name' => $seller_name,
                'seller_href'        => $this->url->link('extension/account/purpletree_multivendor/sellerstore/storeview', 'seller_store_id=' . $seller_list['id']),
                'category_name'  => $data['categories'][(int)$seller_list['category_id']],
                'category_href' => $seller_list['category_id'] == '0' ? $this->url->link('extension/module/companies') : $this->url->link('extension/module/companies', 'search_category=' . $seller_list['category_id']),
                'location_name'  => $data['zones'][(int)$seller_list['store_state']],
                'location_href' => $this->url->link('extension/module/companies', 'search_location=' . $seller_list['store_state']),
                'email' => $seller_list['store_email'],
                'phone' => $seller_list['store_phone']
            );
        }

        $data['filter_name'] = $filter_name;
        $data['filter_location'] = $filter_location;
        $data['filter_category'] = $filter_category;

        $data['action'] = $this->url->link('extension/module/companies');

        $pagination = new Pagination();
        $pagination->total = $seller_totals;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('extension/module/companies','page={page}');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($seller_totals) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($seller_totals - $limit)) ? $seller_totals : ((($page - 1) * $limit) + $limit), $seller_totals, ceil($seller_totals / $limit));

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');


        $this->response->setOutput($this->load->view('extension/module/companies', $data));
    }
}

<?php
class ControllerExtensionModuleSupplierRegistration extends Controller {
    public function index() {
        $this->load->language('extension/module/supplier_registration');

        $this->load->model('extension/module/supplier_registration');
        $this->load->model('tool/image');

        $data['supplier_registration_thumb'] = $this->model_tool_image->resize('catalog/icons/rs-bult.png', 33, 30);

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/supplier_registration', '')
        );

        $data['text_heading'] = $this->language->get('heading_title');
        $data['text_empty'] = $this->language->get('text_empty');

        $this->document->setTitle($this->language->get('heading_title'));
        $data['heading_title'] = $this->language->get('heading_title');

        $limit = '20';

        $filter_data_supplier_registration = array(
            'start'              => ($page - 1) * $limit,
            'limit'              => $limit
        );

        $supplier_registration_totals = $this->model_extension_module_supplier_registration->getTotalSupplierRegistrations($filter_data_supplier_registration);

        $supplier_registration_lists = $this->model_extension_module_supplier_registration->getSupplierRegistrations($filter_data_supplier_registration);

        $data['supplier_registrations'] = array();

        foreach ($supplier_registration_lists as $supplier_registration) {
            $data['supplier_registrations'][] = array(
                'supplier_registration_title' => $supplier_registration['title'],
                'supplier_registration_href'        => $this->url->link('extension/module/supplier_registration/info', 'page=' . $page . '&supplier_registration_id=' . $supplier_registration['supplier_registration_id']),
            );
        }

        $pagination = new Pagination();
        $pagination->total = $supplier_registration_totals;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('extension/module/supplier_registration','page={page}');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($supplier_registration_totals) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($supplier_registration_totals - $limit)) ? $supplier_registration_totals : ((($page - 1) * $limit) + $limit), $supplier_registration_totals, ceil($supplier_registration_totals / $limit));

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');


        $this->response->setOutput($this->load->view('extension/module/supplier_registration', $data));
    }

    public function info() {
        $this->load->language('extension/module/supplier_registration');

        $this->load->model('extension/module/supplier_registration');
        $this->load->model('tool/image');

        if (isset($this->request->get['supplier_registration_id'])) {
            $supplier_registration_id = $this->request->get['supplier_registration_id'];
        } else {
            $this->response->redirect($this->url->link('extension/module/supplier_registration', true));
            return;
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/supplier_registration', 'page='.$page)
        );

        $data['text_heading'] = $this->language->get('heading_title');

        $this->document->setTitle($this->language->get('heading_title'));
        $data['heading_title'] = $this->language->get('heading_title');


        $data['text_supplier_registration'] = $this->language->get('text_supplier_registration');
        $data['text_supplier_registration_title'] = $this->language->get('text_supplier_registration_title');
        $data['text_publish_by'] = $this->language->get('text_publish_by');
        $data['text_publish_date'] = $this->language->get('text_publish_date');
        $data['text_activity_by'] = $this->language->get('text_activity_by');
        $data['text_description'] = $this->language->get('text_description');
        $data['text_back'] = $this->language->get('text_back');

        $supplier_registration = $this->model_extension_module_supplier_registration->getSupplierRegistration($supplier_registration_id);

        $data['supplier_registration_image'] = $this->model_tool_image->resize($supplier_registration['image'], 847, 670);
        $data['supplier_registration_title'] = $supplier_registration['title'];
        $data['supplier_registration_publish_date'] = $supplier_registration['date'];
        $data['supplier_registration_published_by'] = $supplier_registration['published_by'];
        $data['supplier_registration_activity_by'] = $supplier_registration['activity_by'];
        $data['supplier_registration_href'] = $this->url->link('extension/module/supplier_registration/info', 'supplier_registration_id='.$supplier_registration_id);
        $data['back_href'] = $this->url->link('extension/module/supplier_registration', 'page='.$page);


        // Facebook
        $metas[] = array('key' => 'property', 'type' => 'og:title'       , 'content' => $supplier_registration['title']);
        $metas[] = array('key' => 'property', 'type' => 'og:site_name'   , 'content' => $this->config->get('config_name'));
        $metas[] = array('key' => 'property', 'type' => 'og:description' , 'content' => $supplier_registration['title']);
        $metas[] = array('key' => 'property', 'type' => 'og:image'       , 'content' => Journal2Utils::resizeImage($this->model_tool_image, 'catalog/icons/sr.jpg', 328, 280, 'fit'));
        $metas[] = array('key' => 'property', 'type' => 'og:image:width' , 'content' => 328);
        $metas[] = array('key' => 'property', 'type' => 'og:image:height', 'content' => 280);

        $this->journal2->settings->set('share_metas', $metas);


        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');


        $this->response->setOutput($this->load->view('extension/module/supplier_registration_info', $data));
    }
}

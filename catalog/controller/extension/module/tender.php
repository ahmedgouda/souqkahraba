<?php
class ControllerExtensionModuleTender extends Controller {
    public function index() {
        $this->load->language('extension/module/tender');

        $this->load->model('extension/module/tender');
        $this->load->model('tool/image');

        $data['tender_thumb'] = $this->model_tool_image->resize('catalog/icons/tender-bult.png', 33, 30);

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/tender', '')
        );

        $data['text_heading'] = $this->language->get('heading_title');
        $data['text_empty'] = $this->language->get('text_empty');

        $this->document->setTitle($this->language->get('heading_title'));
        $data['heading_title'] = $this->language->get('heading_title');

        $limit = '20';

        $filter_data_tender = array(
            'start'              => ($page - 1) * $limit,
            'limit'              => $limit
        );

        $tender_totals = $this->model_extension_module_tender->getTotalTenders($filter_data_tender);

        $tender_lists = $this->model_extension_module_tender->getTenders($filter_data_tender);

        $data['tenders'] = array();

        foreach ($tender_lists as $tender) {
            $data['tenders'][] = array(
                'tender_title' => $tender['title'],
                'tender_href'        => $this->url->link('extension/module/tender/info', 'page=' . $page . '&tender_id=' . $tender['tender_id']),
            );
        }

        $pagination = new Pagination();
        $pagination->total = $tender_totals;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('extension/module/tender','page={page}');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($tender_totals) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($tender_totals - $limit)) ? $tender_totals : ((($page - 1) * $limit) + $limit), $tender_totals, ceil($tender_totals / $limit));

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');


        $this->response->setOutput($this->load->view('extension/module/tender', $data));
    }

    public function info() {
        $this->load->language('extension/module/tender');

        $this->load->model('extension/module/tender');
        $this->load->model('tool/image');

        if (isset($this->request->get['tender_id'])) {
            $tender_id = $this->request->get['tender_id'];
        } else {
            $this->response->redirect($this->url->link('extension/module/tender', true));
            return;
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/tender', 'page='.$page)
        );

        $data['text_heading'] = $this->language->get('heading_title');

        $this->document->setTitle($this->language->get('heading_title'));
        $data['heading_title'] = $this->language->get('heading_title');


        $data['text_tender'] = $this->language->get('text_tender');
        $data['text_tender_title'] = $this->language->get('text_tender_title');
        $data['text_publish_by'] = $this->language->get('text_publish_by');
        $data['text_publish_date'] = $this->language->get('text_publish_date');
        $data['text_activity_by'] = $this->language->get('text_activity_by');
        $data['text_description'] = $this->language->get('text_description');
        $data['text_back'] = $this->language->get('text_back');

        $tender = $this->model_extension_module_tender->getTender($tender_id);

        $data['tender_image'] = $this->model_tool_image->resize($tender['image'], 847, 670);
        $data['tender_title'] = $tender['title'];
        $data['tender_publish_date'] = $tender['date'];
        $data['tender_published_by'] = $tender['published_by'];
        $data['tender_activity_by'] = $tender['activity_by'];
        $data['tender_href'] = $this->url->link('extension/module/tender/info', 'tender_id='.$tender_id);
        $data['back_href'] = $this->url->link('extension/module/tender', 'page='.$page);


        // Facebook
        $metas[] = array('key' => 'property', 'type' => 'og:title'       , 'content' => $tender['title']);
        $metas[] = array('key' => 'property', 'type' => 'og:site_name'   , 'content' => $this->config->get('config_name'));
        $metas[] = array('key' => 'property', 'type' => 'og:description' , 'content' => $tender['title']);
        $metas[] = array('key' => 'property', 'type' => 'og:image'       , 'content' => Journal2Utils::resizeImage($this->model_tool_image, 'catalog/icons/tender.jpg', 340, 290, 'fit'));
        $metas[] = array('key' => 'property', 'type' => 'og:image:width' , 'content' => 340);
        $metas[] = array('key' => 'property', 'type' => 'og:image:height', 'content' => 290);

        $this->journal2->settings->set('share_metas', $metas);

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');


        $this->response->setOutput($this->load->view('extension/module/tender_info', $data));
    }
}

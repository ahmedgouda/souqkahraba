<?php
class ControllerExtensionAccountPurpletreeMultivendorSellercontact extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('purpletree_multivendor/sellercontact');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('extension/purpletree_multivendor/sellercontact');
		$data['seller_id'] = $this->request->get['seller_id'];	
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$this->model_extension_purpletree_multivendor_sellercontact->addContact($this->request->post);
			
			$seller_email = $this->model_extension_purpletree_multivendor_sellercontact->getStoreEmail($this->request->post['seller_id']);

//			$mail = new Mail($this->config->get('config_mail_engine'));
//			$mail->protocol = $this->config->get('config_mail_protocol');
//			$mail->parameter = $this->config->get('config_mail_parameter');
//			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
//			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
//			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
//			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
//			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
//
//			$mail->setTo($seller_email);
//			$mail->setFrom($this->request->post['customer_email']);
//			$mail->setSender(html_entity_decode($this->request->post['customer_name'], ENT_QUOTES, 'UTF-8'));
//			$mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $this->request->post['customer_name'].'-'.$this->request->post['telephone']), ENT_QUOTES, 'UTF-8'));
//			$mail->setText($this->request->post['customer_message']);
//			$mail->send();

			$mail = new Mail($this->config->get('config_mail_engine'));
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$data['cust_name'] = $this->request->post['customer_name'];
			$data['cust_email'] = $this->request->post['customer_email'];
			$data['cust_phone'] = $this->request->post['telephone'];
			$data['cust_message'] = $this->request->post['customer_message'];

			$mail->setTo($seller_email);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->request->post['customer_name'], ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $this->request->post['customer_name']), ENT_QUOTES, 'UTF-8'));
//			$mail->setText($this->request->post['customer_message']);
			$mail->setHtml($this->load->view('mail/contact_seller', $data));
			$mail->send();
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('extension/account/purpletree_multivendor/sellercontact&seller_id='.$data['seller_id']));
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/account/purpletree_multivendor/sellercontact&seller_id='.$data['seller_id'])
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_enquiry'] = $this->language->get('entry_enquiry');


		if (isset($this->error['customer_name'])) {
			$data['error_name'] = $this->error['customer_name'];
		} else {
			$data['error_name'] = '';
		}

        if (isset($this->error['telephone'])) {
            $data['error_telephone'] = $this->error['telephone'];
        } else {
            $data['error_telephone'] = '';
        }

		if (isset($this->error['customer_email'])) {
			$data['error_email'] = $this->error['customer_email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['customer_message'])) {
			$data['error_enquiry'] = $this->error['customer_message'];
		} else {
			$data['error_enquiry'] = '';
		}
		
		if (isset($this->error['error_warning'])) {
			$data['error_warning'] = $this->error['error_warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['button_submit'] = $this->language->get('button_submit');

		$data['action'] = $this->url->link('extension/account/purpletree_multivendor/sellercontact&seller_id='.$data['seller_id'], '', true);

		if (isset($this->request->post['customer_name'])) {
			$data['customer_name'] = $this->request->post['customer_name'];
		} else {
			$data['customer_name'] = '';
		}

        if (isset($this->request->post['customer_email'])) {
            $data['telephone'] = $this->request->post['telephone'];
        } else {
            $data['telephone'] = '';
        }

		if (isset($this->request->post['customer_email'])) {
			$data['customer_email'] = $this->request->post['customer_email'];
		} else {
			$data['customer_email'] = '';
		}

		if (isset($this->request->post['customer_message'])) {
			$data['customer_message'] = $this->request->post['customer_message'];
		} else {
			$data['customer_message'] = '';
		}

		// Captcha
		$data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'), $this->error);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/purpletree_multivendor/sellercontact', $data));
	}

	protected function validate() {
		if(!$this->customer->validateSeller()) {
			$this->error['error_warning'] = $this->language->get('error_license');
		}
		if ((utf8_strlen($this->request->post['customer_name']) < 3) || (utf8_strlen($this->request->post['customer_name']) > 32)) {
			$this->error['customer_name'] = $this->language->get('error_name');
		}

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

		if (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['customer_email'])) {
			$this->error['customer_email'] = $this->language->get('error_email');
		}

		if ((utf8_strlen($this->request->post['customer_message']) < 10) || (utf8_strlen($this->request->post['customer_message']) > 3000)) {
			$this->error['customer_message'] = $this->language->get('error_enquiry');
		}

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('contact', (array)$this->config->get('config_captcha_page'))) {
			$captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

			if ($captcha) {
				$this->error['captcha'] = $captcha;
			}
		}

		return !$this->error;
	}
	
	public function sellercontactlist(){
		
		$this->load->model('extension/purpletree_multivendor/sellercontact');
		
		$this->load->language('purpletree_multivendor/sellercontact');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_empty_result'] = $this->language->get('text_empty_result');
		
		if($this->customer->isSeller()){
			
			if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
			} else {
				$page = 1;
			}	
			
			if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
			} else {
				$limit = 10;
			}
			$seller_id = $this->customer->getId();
			
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);
			
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_heading'),
				'href' => $this->url->link('extension/account/purpletree_multivendor/sellercontact/sellercontactlist', '', true)
			);
			$data['text_heading'] = $this->language->get('text_heading');
			$filter_data = array(
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit,
				'seller_id' 		=> $seller_id
			);
				
			$contact_total = $this->model_extension_purpletree_multivendor_sellercontact->getTotalSellerContact($filter_data);

			$results = $this->model_extension_purpletree_multivendor_sellercontact->getSellerContact($filter_data);
			
			$data['sellercontacts'] = array();
			
			if ($results) {
				foreach($results as $result){
					$data['sellercontacts'][] = array(
						'customer_name'     => $result['customer_name'],
                        'customer_email'     => $result['customer_email'],
                        'telephone'     => $result['telephone'],
						'customer_message'       => nl2br($result['customer_message']),
						'date_added' => date($this->language->get('date_format_short'), strtotime($result['created_at']))
					);
				}
			}
			
			$pagination = new Pagination();
			$pagination->total = $contact_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('extension/account/purpletree_multivendor/sellercontact/sellercontactlist', 'seller_id=' . $seller_id . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($contact_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($contact_total - $limit)) ? $contact_total : ((($page - 1) * $limit) + $limit), $contact_total, ceil($contact_total / $limit));
				
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('account/purpletree_multivendor/contactlist', $data));
		}
	}
}
